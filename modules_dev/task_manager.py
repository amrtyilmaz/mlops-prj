# task_manager
import pandas as pd
import numpy as np
import datetime as dt
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler
from sklearn.pipeline import Pipeline
import pickle
import warnings
warnings.filterwarnings("ignore")

import logging
from logging import Formatter, StreamHandler, FileHandler


def init_log(task_name: str, log_level: str) -> logging.Logger:
    """
    Initialize a logger for a given task_name and log_level.

    Args:
        task_name (str): Name of the task.
        log_level (int): Level of logging.

    Returns:
        logger (logging.Logger): Instance of the logger.
    """
    logger = logging.getLogger(task_name)

    stream_handler = StreamHandler()

    file_handler = FileHandler("test_logs.txt")

    file_handler.setFormatter(Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s"))
    stream_handler.setFormatter(Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s"))

    logger.setLevel(log_level)

    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)
    return logger

logger = init_log("Task_Manager", log_level=logging.DEBUG)


class PrePocessManager:
    """
    Class for preprocessing data for training models.
    """

    def __init__(self):
        self.data_path = "https://raw.githubusercontent.com/amankharwal/Website-data/master/supplement.csv"
        self.categorical_columns = [
            'store_type', 'store_id',
            'location_type', 'region_code',
            'holiday', 'discount',
            'year', 'month',
            'day', 'week', 'weekend',
            'day_of_week', 'quarter'
        ]
        self.numerical_columns = ['sales']
        self.scaler = MinMaxScaler()
        logger.info("PrePocessManager initiated...")

    def get_data(self) -> pd.DataFrame:
        """
        Get raw data from URL and pre-process.

        Returns:
            df_raw (pd.DataFrame): Processed DataFrame.
        """
        try:
            data = pd.read_csv(self.data_path)
            data.columns = data.columns.str.lower()
            df_raw = data.sample(frac=0.05)
            data.drop_duplicates(keep="firs", inplace=True)
            self.df_raw = df_raw.copy()
            return df_raw
        except Exception as err:
            logger.error(f"get_data function error :{err}")


    def get_new_features(self) -> pd.DataFrame:
        """
        Generate new features based on existing data.

        Returns:
            df_features (pd.DataFrame): Processed DataFrame with new features.
        """
        df_features = self.get_data()
        df_features['date'] = pd.to_datetime(df_features['date'])
        df_features['year'] = df_features['date'].dt.year
        df_features['month'] = df_features['date'].dt.month
        df_features['day'] = df_features['date'].dt.day
        df_features['week'] = df_features['date'].dt.week
        df_features['weekend'] = df_features['week'].apply(lambda x: 1 if x in [6,7] else 0)
        df_features['day_of_week'] = df_features['date'].dt.dayofweek
        df_features['quarter'] = df_features['date'].dt.quarter
        df_features = df_features.drop(["id","date"], axis=1)
        ##change sales column index
        df_features.insert(0, '#order', df_features.pop("#order"))
        ##rename sales as target_variable
        df_features.columns = df_features.columns.str.replace('#order', 'target_variable')
        logger.debug(f"df_features.columns : {df_features.columns}")

        return df_features

    def add_normalize_numericals(self) -> pd.DataFrame:
        """
        Adds numerical columns to the normalized dataframe and scales them.

        Args:
            None.

        Returns:
            DataFrame: A pandas dataframe with added and normalized numerical columns.
        """
        df_normalized = self.get_new_features()
        df_normalized[self.numerical_columns] = self.df_raw[self.numerical_columns].values
        df_normalized[self.numerical_columns] = self.scaler.fit_transform(df_normalized[self.numerical_columns])
        logger.debug("Preprocessing finished.")
        return df_normalized

    def run(self) -> pd.DataFrame:
        """
        Executes the preprocessor.

        Args:
            None.

        Returns:
            DataFrame: A pandas dataframe with added and normalized numerical columns.
        """
        return self.add_normalize_numericals()


class ModelManager:
    def __init__(self, dataset: pd.DataFrame):
        """
        ModelManager is a class that trains a random forest regressor on a given dataset and saves the model.

        Args:
            dataset (pd.DataFrame): The dataset to train the model on.
            It should contain the features and target_variable.

        Returns:
            None
        """
        self.dataset = dataset

    def train_pipeline(self) -> None:
        """
        Trains a random forest regressor on a given dataset and saves the model.

        Returns:
            None
        """
        logger.info("Training pipeline initialized.")
        categorical_columns = [
            'store_id', 'store_type',
            'location_type', 'region_code',
            'holiday', 'discount',
            'year', 'month',
            'day', 'week', 'day_of_week', 'quarter'
        ]

        numerical_columns = ['sales']

        # Create a ColumnTransformer with a OneHotEncoder for categorical columns, and a MinMaxScaler for numerical columns
        ct = ColumnTransformer([
            ('one_hot', OneHotEncoder(categories='auto', handle_unknown='ignore'), categorical_columns),
            ('scaler', MinMaxScaler(), numerical_columns)
        ])

        regressor = RandomForestRegressor(
                n_estimators = 1000,
                max_features = 'sqrt',
                max_depth = 10,
                random_state = 18
            )

        # Create a Pipeline with a ColumnTransformer and a Regression Model
        pipeline = Pipeline([
            ('transformer', ct),
            ('regressor', regressor)
        ])

        # Fit the model to the data
        self.model = pipeline.fit(self.dataset, self.dataset['target_variable'])
        logger.info('Model fitted successfully.')

        # Use pickle to save model for next usage.
        self.filename = 'model_v1.pk'
        with open('./'+self.filename, 'wb') as file:
            pickle.dump(pipeline, file)
        logger.info(f'Model saved successfully as {self.filename}')


    def get_metrics(self) -> None:
        """
        Predicts using the saved model and prints evaluation metrics

        Returns:
            None
        """
        y_pred = self.model.predict(self.X_test)
         # Calculate MSE
        mse = mean_squared_error(self.y_test, y_pred)
        # Calculate RMSE
        rmse = np.sqrt(mean_squared_error(self.y_test, y_pred))
        # Calculate r2_score
        r2 = r2_score(self.y_test, y_pred)
        # Print the results
        print(f"Results for {self.model_name}")
        print("Mean Squared Error: ", mse)
        print("Root Mean Squared Error: ", rmse)
        print("r2_score: ", r2)


    def load_model_pipeline(self):
        with open('./'+self.filename, 'rb') as f:
            loaded_model = pickle.load(f)
        logger.info(f"{self.filename} loaded as model")
        return loaded_model
