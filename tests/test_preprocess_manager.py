import unittest
import pandas as pd
from managers.preprocess_manager import PreProcessManager

class TestPreProcessManager(unittest.TestCase):

    def setUp(self):
        self.preprocess_manager = PreProcessManager()

    def test_get_data(self):
        # df_raw = self.preprocess_manager.get_data()
        df_raw = pd.DataFrame.from_dict(
            {'id': ['T1105738', 'T1185366', 'T1170529'],
            'store_id': [267, 212, 331],
            'store_type': ['S4', 'S4', 'S1'],
            'location_type': ['L2', 'L2', 'L1'],
            'region_code': ['R1', 'R3', 'R4'],
            'date': ['2018-10-17', '2019-05-23', '2019-04-13'],
            'holiday': [1, 0, 1],
            'discount': ['No', 'Yes', 'No'],
            '#order': [66, 110, 49],
            'sales': [37706.22, 70143.0, 25310.85]},
            orient="index"
        )
        self.assertIsInstance(df_raw, pd.DataFrame)
        self.assertTrue(df_raw.shape[0] > 0)
        self.assertTrue(df_raw.shape[1] > 0)
        # self.assertTrue(df_raw.loc[0, "holiday"] == 1)

    def test_get_new_features(self):
        df_features = self.preprocess_manager.get_new_features()
        self.assertIsInstance(df_features, pd.DataFrame)
        self.assertTrue(df_features.shape[0] > 0)
        self.assertTrue(df_features.shape[1] > 0)
        self.assertIn('target_variable', df_features.columns)
        self.assertIn('quarter', df_features.columns)
        self.assertIn('day_of_week', df_features.columns)

    def test_add_normalize_numericals(self):
        normalized = self.preprocess_manager.add_normalize_numericals()
        self.assertIsInstance(normalized, pd.DataFrame)
        self.assertTrue(normalized.shape[0] > 0)
        self.assertTrue(normalized.shape[1] > 0)
        self.assertIn('target_variable', normalized.columns)

    def test_run(self):
        processed_data = self.preprocess_manager.run()
        self.assertIsInstance(processed_data, pd.DataFrame)
        self.assertTrue(processed_data.shape[0] > 0)
        self.assertTrue(processed_data.shape[1] > 0)
        self.assertIn('target_variable', processed_data.columns)

