# mlops-tutorial

[Train them and they will leave!](https://www.inroads.co.in/blogs/2015/3/6/train-them-and-they-will-leave)


## Week 1

**Topic**

- Develop a model for choosen data on a jupyter/colab etc. notebook.
- Convert preprocessing code into module.

**To-Dos for all until next week**
- Add label_encoder to preprocessing module
- Add another scaler to preprocessing module


## Week 2 - 20.01.2023

**Topic**

- Use venv for local development
- Convert model codes into modules. see
  [example](https://towardsdatascience.com/3-ways-to-deploy-machine-learning-models-in-production-cdba15b00e)

### Assignments:

- Categorical Value Encoding : Ahmet


## Week 3 - 26.01.2023

**Topic**

- Model save-load and inference modules - postponed

**To-Dos for all until next week**

- Generate model-load and prediction module module - postponed
- Add label_encoder to preprocessing module
- Add another scaler to preprocessing module

#### Assignments :

- Numerical Value Scaling : Halil -10dk
- Convert problem to classification using feature discretization : Cabir max 5dk
- Model Evaluation : regression (mse, rmsq, r2), classification(f1, recall, precision, auc, roc curve) : Gursev 15 dk


## Week 4

**Topic**

- Model save-load and inference - Done

**To-Dos for all until next week**

- Generate model save-load modules
- Generate model inference pipeline and module

#### Assignments :

- Hashing encoding and Binary encoding implementation using our dataset : Ahmet max 5dk - Skipped
- Model Explainability: Feature Importance using SHAP + LIME : Bunyamin 15 dk - Skipped
- Outlier Handling: Meryem - Partly done
- Feature Crossing : Aydin - Skipped

#### Bonus
- DictVectorizer - Informed verbally


## Week 5

**Topic**

- Logging

**To-Dos for all until next week**

- At least 1 medium article should be read for each topic
#### Assignments :

- Feature Crossing : Aydin

## Week 6 - 16.02.2023

**Topic**

- Logging
- Linting

#### Assignments :

- Model Explainability: Feature Importance using SHAP + LIME : Bunyamin 15 dk
- Dimentionality Reduction Gokhan-Damla


## Week 7

**Topic**

- Unit Test

#### Assignments :

- Model Explainability: Feature Importance using SHAP + LIME : Bunyamin 15 dk - skipped
- Dimentionality Reduction Damla - skipped


## Week 8

**Topic**

- FAST API

#### Assignments :

- model_manager.py unittest : Halil

- Model Explainability: Feature Importance using SHAP + LIME : Bunyamin 15 dk
- Dimentionality Reduction Damla

## Upcoming/Handled Topics

- Logging +
- Linting +
- CI/CD Pipeline +
  -[CI/CD tutorial](https://www.youtube.com/watch?v=HGJWMTNeYqI&list=RDCMUCUUl_HXJjU--iYjUkIgEcTw&index=7&ab_channel=ValentinDespa)
- Unit testing +
- FAST API
- Containerization - Docker
- Deployment - AWS
- Performance Testing
- Code Optimization
- Monitoring and alerting (theoretically)
- Kubernetes integration (theoretically)
- MLflow-Prefect-BentoML


## Resources
### Repos

[MLOps-ZoomCamp](https://github.com/DataTalksClub/mlops-zoomcamp)
[Data Engineering](https://github.com/DataTalksClub/data-engineering-zoomcamp)

### Articles

[Medium - Good Data Scientists Write Good Code](https://towardsdatascience.com/good-data-scientists-write-good-code-28352a826d1f)

### Websites

[NeptuneAI-MLOps Blog](https://neptune.ai/blog)
[Machine Learning Mastery](https://machinelearningmastery.com/)

### Courses
[madewithml](https://madewithml.com/#mlops)
[docker](https://www.udemy.com/course/adan-zye-docker/)


### Books
[Designing Machine Learning Systems](https://www.oreilly.com/library/view/designing-machine-learning/9781098107956/)

### Code
[Hidden Python Libraries](https://medium.com/@cognizone/what-is-an-enterprise-knowledge-graph-41e98826de7e)