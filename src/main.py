# Import necessary libraries
import logging
from fastapi import FastAPI, Request
from pydantic import BaseModel
from typing import List
import pandas as pd
import uvicorn
import warnings
warnings.filterwarnings("ignore")

from managers.model_manager import ModelManager
from managers.preprocess_manager import PreProcessManager
from managers.log_manager import init_log


logger = init_log("Prediction API", log_level=logging.INFO)

# Create an instance of the FastAPI
app = FastAPI()

# Define a request model
class PredictionRequest(BaseModel):
    id: str = "T1057483"
    store_id: str = "104"
    store_type: str = "S1"
    location_type: str = "L1"
    region_code: str = "R2"
    holiday: int = 1
    discount: str = "No"
    date: str = "2018-06-07"
    sales:int = 33414


# Define a response model
class PredictionResponse(BaseModel):
    prediction: float


# Load and preprocess the training data
preprocess_manager = PreProcessManager()
model_manager = ModelManager()


# Train and save the model using the preprocessed data
def train_load_model():
    processed_train_data = preprocess_manager.preprocess_training_data()
    model_manager.train_pipeline(processed_train_data)
    loaded_model = model_manager.load_model_pipeline()
    return loaded_model

loaded_model = train_load_model()


# Define an endpoint to visualize model accuracies
@app.get("/metrics")
async def plot_accuracy(request: Request):
    metrics = model_manager.get_metrics()
    # Return the plot as a response
    return metrics


# Define an endpoint to make predictions
@app.post("/predict", response_model=PredictionResponse)
async def predict(request: Request, inputs: List[PredictionRequest]):

    logger.info(f"Given values for prediction: {inputs}")
    # Convert input data into a pandas dataframe
    inputs_df = pd.DataFrame([input.dict() for input in inputs])

    # Preprocess the input data
    inputs_df = preprocess_manager.preprocess_prediction_data(inputs_df)

    # Make a prediction using the loaded model
    try:
        prediction = loaded_model.predict(inputs_df)
        # Return the prediction as a response
        return {"prediction": round(prediction.tolist()[0], 2)}
    except Exception as e:
        logger.error(f"Error occurred while predicting: {e}")
        return {"prediction": "Error occurred while predicting."}


# Run the API with the uvicorn server
if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=False)
