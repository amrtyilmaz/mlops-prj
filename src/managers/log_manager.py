import logging
import sys

logger = None

def init_log(name, log_level=logging.INFO):
    global logger
    if logger is None:
        logger = logging.getLogger(name)
        logger.setLevel(log_level)

        # Create console handler and set its log level
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(log_level)

        # Create a formatter for the console handler
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # Add the formatter to the console handler
        ch.setFormatter(formatter)

        # Add the console handler to the logger
        logger.addHandler(ch)

    return logger
