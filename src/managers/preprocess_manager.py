# preprocess_manager
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import MinMaxScaler
import warnings
warnings.filterwarnings("ignore")
import logging
from managers.log_manager import init_log

logger = init_log("PreProcess_Manager", log_level=logging.INFO)

class PreProcessManager:
    data_path = "https://raw.githubusercontent.com/amankharwal/Website-data/master/supplement.csv"

    def __init__(self):
        self.categorical_columns = [
            'store_type', 'store_id',
            'location_type', 'region_code',
            'holiday', 'discount',
            'year', 'month',
            'day', 'week', 'weekend',
            'day_of_week', 'quarter'
        ]
        self.numerical_columns = ['sales']
        self.df_raw = None
        logger.info("PreProcessManager initiated...")

    def get_data(self) -> pd.DataFrame:
        """
        Get raw data from URL and pre-process.

        Returns:
            df_raw (pd.DataFrame): Processed DataFrame.
        """
        try:
            data = pd.read_csv(PreProcessManager.data_path)
            data.columns = data.columns.str.lower()
            df_raw = data.sample(frac=0.05)
            df_raw.drop_duplicates(keep="first", inplace=True)
            return df_raw
        except Exception as err:
            logger.error(f"get_data function error: {err}")

        return df_raw

    def get_new_features(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        Generate new features based on existing data.

        Args:
            df (pd.DataFrame): Input DataFrame to generate features from.

        Returns:
            df_features (pd.DataFrame): Processed DataFrame with new features.
        """
        df_features = df.copy()
        df_features['date'] = pd.to_datetime(df_features['date'])
        df_features['year'] = df_features['date'].dt.year
        df_features['month'] = df_features['date'].dt.month
        df_features['day'] = df_features['date'].dt.day
        df_features['week'] = df_features['date'].dt.week
        df_features['weekend'] = df_features['week'].apply(lambda x: 1 if x in [6,7] else 0)
        df_features['day_of_week'] = df_features['date'].dt.dayofweek
        df_features['quarter'] = df_features['date'].dt.quarter
        df_features = df_features.drop(["id","date"], axis=1)
        if not self.prediction:
            df_features.insert(0, '#order', df_features.pop("#order"))
            df_features.columns = df_features.columns.str.replace('#order', 'target_variable')
            df_features = df_features[['store_id', 'store_type', 'location_type',
       'region_code', 'holiday', 'discount', 'sales', 'year', 'month', 'day',
       'week', 'weekend', 'day_of_week', 'quarter','target_variable']]
        logger.debug(f"df_features.columns: {df_features.columns}")
        return df_features

    def preprocess_training_data(self) -> pd.DataFrame:
        """
        Preprocesses the training data.

        Returns:
            df_normalized (pd.DataFrame): Preprocessed training data.
        """
        logger.info("Preprocessing for training data started.")
        df_raw = self.get_data()
        self.prediction = False
        df_features = self.get_new_features(df_raw)
        df_features = df_features[['store_id', 'store_type', 'location_type',
       'region_code', 'holiday', 'discount', 'sales', 'year', 'month', 'day',
       'week', 'weekend', 'day_of_week', 'quarter', 'target_variable']]
        df_normalized = df_features.copy()
        self.scaler = MinMaxScaler()
        df_normalized[self.numerical_columns] = self.scaler.fit_transform(df_normalized[self.numerical_columns])
        logger.info("Preprocessing for training data finished.")
        logger.debug("Training data columns:", df_normalized.columns)
        return df_normalized

    def preprocess_prediction_data(self, inputs_df: pd.DataFrame) -> pd.DataFrame:
        """
        Preprocesses the input data for prediction.

        Args:
            inputs_df (pd.DataFrame): Input DataFrame to preprocess.

        Returns:
            inputs_df (pd.DataFrame): Preprocessed input data for prediction.
        """
        logger.info("Preprocessing for prediction data started.")
        self.prediction = True
        inputs_df = self.get_new_features(inputs_df)
        inputs_df[self.numerical_columns] = self.scaler.transform(inputs_df[self.numerical_columns])
        logger.info("Preprocessing for prediction data finished.")
        logger.debug("Prediction data columns:", inputs_df.columns)
        return inputs_df
