# model_manager
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler
from sklearn.pipeline import Pipeline
import pickle
import warnings
from typing import Dict
warnings.filterwarnings("ignore")

import logging
from managers.log_manager import init_log

logger = init_log("Model_Manager", log_level=logging.INFO)

class ModelManager:
    def __init__(self):
        """
        ModelManager is a class that trains a random forest regressor on a given dataset and saves the model.

        Args:
            dataset (pd.DataFrame): The dataset to train the model on.
            It should contain the features and target_variable.

        Returns:
            None
        """

    def train_pipeline(self, dataset) -> None:
        """
        Trains a random forest regressor on a given dataset and saves the model.

        Returns:
            None
        """
        logger.info("Training pipeline initialized.")
        categorical_columns = [
            'store_id', 'store_type',
            'location_type', 'region_code',
            'holiday', 'discount',
            'year', 'month',
            'day', 'week', 'day_of_week', 'quarter'
        ]

        numerical_columns = ['sales']

        # Create a ColumnTransformer with a OneHotEncoder for categorical columns, and a MinMaxScaler for numerical columns
        ct = ColumnTransformer(
            [('one_hot', OneHotEncoder(categories='auto', handle_unknown='ignore'), categorical_columns),
            ('scaler', MinMaxScaler(), numerical_columns)]
        )

        self.model_name = "RandomForestRegressor"
        regressor = RandomForestRegressor(
                n_estimators = 1000,
                max_features = 'sqrt',
                max_depth = 5,
                random_state = 18
        )

        # Create a Pipeline with a ColumnTransformer and a Regression Model
        pipeline = Pipeline([
            ('transformer', ct),
            ('regressor', regressor)
        ])

        # Fit the model to the data
        X_train, self.X_test, y_train, self.y_test = train_test_split(dataset.drop(['target_variable'], axis=1),
                                                            dataset['target_variable'], test_size=0.2, random_state=42)
        # y = dataset['target_variable']
        # X = dataset.drop('target_variable', axis=1)

        self.model = pipeline.fit(X_train, y_train)
        logger.info('Model fitted successfully.')

        # Use pickle to save model for next usage.
        self.filename = 'model_v1.pk'

        with open('./'+self.filename, 'wb') as file:
            pickle.dump(pipeline, file)

        logger.info(f'Model saved successfully as {self.filename}')


    def get_metrics(self) -> Dict[str, float]:
        """
        Predicts using the saved model and returns evaluation metrics as a dictionary

        Returns:
            A dictionary containing the evaluation metrics
        """

        y_pred = self.model.predict(self.X_test)
        # Calculate MSE
        mse = mean_squared_error(self.y_test, y_pred)
        # Calculate RMSE
        rmse = np.sqrt(mean_squared_error(self.y_test, y_pred))
        # Calculate r2_score
        r2 = r2_score(self.y_test, y_pred)
        # Return the results as a dictionary
        return {
            "model_name": self.model_name,
            "mean_squared_error": round(mse, 2),
            "root_mean_squared_error": round(rmse, 2),
            "r2_score": round(r2, 2)
        }


    def load_model_pipeline(self):

        with open('./'+self.filename, 'rb') as f:
            loaded_model = pickle.load(f)

        logger.info(f"{self.filename} loaded as model")

        return loaded_model
